const std = @import("std");
const Allocator = std.mem.Allocator;
const assert = std.debug.assert;

const vendor = @import("vendor/vendor.zig");
const DynArray = vendor.DynArray;

/// Run lenth encoding implementation,
/// Usage:
///  `push` your items and when you need to get the item at an index,
///  call `getItem` with the index, and it will return your item at that index. 
pub fn RLE(comptime T: type) type {
    return struct {
        const Self = @This();

        const lens_t = DynArray(u16);
        const runs_t = DynArray(T);

        lens: lens_t,
        runs: runs_t,

        pub fn init(allocator: *Allocator) Self {
            return Self {
                .lens = lens_t.init(allocator),
                .runs = runs_t.init(allocator),
            };
        }

        pub fn deinit(self: Self) void {
            self.lens.deinit();
            self.runs.deinit();
        }

        pub fn getItem(self: Self, index: usize) T {
            assert(self.lens.items.len == self.runs.items.len);

            var len_index: usize = 0;
            var total_len = self.lens.items[len_index];

            while (index > total_len) {
                len_index += 1;
                total_len += self.lens.items[len_index];
            }

            return self.runs.items[len_index];
        }

        pub fn push(self: *Self, item: T) !void {
            if (self.runs.last()) |last| {
                if (last.* == item) {
                    self.lens.lastUnchecked().* += 1;
                    return;
                }
            }

            try self.runs.push(item);
            try self.lens.push(1);
        }
    };
}
