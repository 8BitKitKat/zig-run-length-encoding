# Zig Run Length Encoding

A [Run Length Encoding (RLE)](https://en.wikipedia.org/wiki/Run-length_encoding) implementation in zig

# Usage

```zig
var rle = RLE(u8).init(allocator);
defer rle.deinit();

try rle.push('W');
try rle.push('W');
try rle.push('W');
try rle.push('W');

try rle.push('B');

try rle.push('W');
try rle.push('W');
try rle.push('W');
try rle.push('W');

try rle.push('B');
try rle.push('B');
try rle.push('B');

try rle.push('W');
try rle.push('W');

const x1 = rle.getItem(3);  // 'W'
const x2 = rle.getItem(5);  // 'B'
const x4 = rle.getItem(11); // 'B'

```
